---
title: Invalid JSON in Events fact
description: 
published: true
date: 2023-02-14T21:26:49.381Z
tags: 
editor: markdown
dateCreated: 2023-02-14T21:26:46.473Z
---

# Invalid JSON Events Fact

```sql
select
  date_trunc('day', datecreated) as "day",
  category,
  name as event_name,
  count(*) as total_rows,
  count(case when detail is null then id end) as null_detail,
  count(case when is_valid_json(detail) and can_json_parse(detail) then id end) as valid_json,
  count(case when is_valid_json(detail) is false or can_json_parse(detail) is false then id end) as invalid_json
from "zenfolio_userevents_production_zf"."gauserevent"
where
--   category ilike '%onboarding%' and
  datecreated > '2023-01-09'
group by 1, 2, 3
having invalid_json > 0
order by 1 desc, 3, 2
```