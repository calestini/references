---
title: Real Retina Wiki
description: Place to store references, notes and internal memos
published: true
date: 2022-09-23T20:30:36.411Z
tags: 
editor: markdown
dateCreated: 2022-09-21T00:12:13.533Z
---

# Real Retina Wiki

Place to store notes, descriptions, references and comments, as needed

## Examples
- Useful queries for debugging applications
- Useful documentation for git / CI with our clients
- How-to's:
	- Setting up a server
  - Setting up nginx configuration
  - Creating SSL certificates