---
title: Copy file from image
description: 
published: true
date: 2023-01-24T04:33:18.213Z
tags: 
editor: markdown
dateCreated: 2023-01-24T04:32:29.794Z
---

# Copy file from image


- [Souce](https://stackoverflow.com/questions/25292198/docker-how-can-i-copy-a-file-from-an-image-to-a-host)

```
docker cp $(docker create --name tc registry.example.com/ansible-base:latest):/home/ansible/.ssh/id_rsa ./hacked_ssh_key && docker rm tc
```