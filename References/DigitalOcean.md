---
title: Digital Ocean
description: 
published: true
date: 2022-10-23T18:08:21.928Z
tags: 
editor: markdown
dateCreated: 2022-10-23T18:08:18.771Z
---

# Digital Ocean

## Launching an Instance

https://gitlab.com/calestini/torneo/-/blob/master/Notes.md

- Login as root
- Change to only login without password
```
nano /etc/ssh/sshd_config

 #change the following line:
 PermitRootLogin without-password
```

- Stop pid running

```
ps auxw | grep ssh
 USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
 root       681  0.0  0.1  49948  2332 ?        Ss    2012   3:23 /usr/sbin/sshd -D

 #restart
 kill -HUP 681
```

- Add new user
```
 adduser <username>
 
 # make all priviledges
  usermod -aG sudo <username>
```

- Login as user

```
 #change user to new one:
 su - <username>

 #create directory and files for keys
 mkdir ~/.ssh
 chmod 700 ~/.ssh
 touch ~/.ssh/authorized_keys
 nano ~/.ssh/authorized_keys
 # (paste the key here and save with CTRL+X --> Y --> ENTER)

 chmod 600 ~/.ssh/authorized_keys
 exit
 exit

```