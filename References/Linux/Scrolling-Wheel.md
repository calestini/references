---
title: Adjusting Mouse Scrolling Wheel
description: 
published: true
date: 2022-09-23T22:13:15.683Z
tags: 
editor: markdown
dateCreated: 2022-09-23T20:46:47.896Z
---

# Adjusting Mouse Scrolling Wheel

Maybe using imwheel?

- https://askubuntu.com/questions/285689/increase-mouse-wheel-scroll-speed
- Oficial docs: https://wiki.archlinux.org/title/IMWheel
- Status: seems to work ok