---
title: Check Largest Directories in Linux
description: 
published: true
date: 2023-01-06T02:07:21.999Z
tags: 
editor: markdown
dateCreated: 2023-01-06T02:07:19.101Z
---

# Check Largest Directories in Linux


```bash
du -a /var | sort -n -r | head -n 10
```