---
title: Delete Unused Storage
description: 
published: true
date: 2023-01-14T00:27:09.627Z
tags: 
editor: markdown
dateCreated: 2023-01-14T00:27:06.651Z
---

# Delete Unused Storage

- Journald

Edit file `/etc/systemd/journald.conf`

Set `SystemMaxUse=100M` (for 100MB of log data)

Restart 

```
sudo service systemd-journald restart
```