---
title: Nginx
description: Helpful notes and helpers for managing nginx
published: true
date: 2022-09-21T00:45:56.898Z
tags: nginx
editor: markdown
dateCreated: 2022-09-21T00:40:46.750Z
---

# NGINX

<Pronounce: Engine-X>

- Check syntax
```
sudo nginx -t
```

- Restart nginx
```
sudo systemctl nginx
```

