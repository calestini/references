---
title: Redis Flush Cache
description: 
published: true
date: 2023-02-10T03:30:09.761Z
tags: 
editor: markdown
dateCreated: 2023-02-10T03:30:06.642Z
---

# Flush Redis Cache

```
docker exec -it backend_redis_1 redis-cli
auth [redis password]
FLUSHALL
```