---
title: JSON To String Representation
description: 
published: true
date: 2023-03-18T00:35:13.787Z
tags: 
editor: markdown
dateCreated: 2023-03-18T00:07:06.240Z
---

# Transform JSON to Array of String of its Properties


```js
let x = {a: {b: 1, c: 2}, d: null, e: {f: {g: 7}}}


function jsonToArrayOfString(initialObj){
	
  let results = [];

	function recursiveLogObj(obj, location=''){
  	let loc;
    for (let k of Object.keys(obj)){
      loc = location ? location +'.'+k: k;
      if (obj[k] && typeof obj[k] === 'object'){
        recursiveLogObj(obj[k], loc)
      } else {
      	// if string, number, array, null, it falls here. 
        // Biggest concern is the array
      	results.push(loc)
      }
    }
    return results
  }
  return recursiveLogObj(initialObj, '')
}
```