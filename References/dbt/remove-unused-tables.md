---
title: Remove Unused Tables
description: 
published: true
date: 2023-04-27T23:09:05.426Z
tags: 
editor: markdown
dateCreated: 2023-04-27T23:09:02.488Z
---

# Remove Unused Tables


```jinja2
{% macro delete_orphaned_tables(schema=['dbt_lucas'], dry_run=True) %}
  {% if (schema is not string and schema is not iterable) or schema is mapping or schema|length <= 0 %}
    {% do exceptions.raise_compiler_error('"schema" must be a string or a list') %}
  {% endif %}

  {% call statement('get_orphaned_tables', fetch_result=True) %}
    SELECT current.schema_name,
           current.ref_name,
           current.ref_type
    FROM (
      SELECT schemaname AS schema_name,
             tablename  AS ref_name,
             'table'    AS ref_type
      FROM pg_catalog.pg_tables pt
      WHERE schemaname IN (
        {%- if schema is iterable and (var is not string and var is not mapping) -%}
          {%- for s in schema -%}
            '{{ s }}'{% if not loop.last %},{% endif %}
          {%- endfor -%}
        {%- elif schema is string -%}
          '{{ schema }}'
        {%- endif -%}
      )
      UNION ALL
      SELECT schemaname AS schema_name,
             viewname   AS ref_name,
             'view'     AS ref_type
      FROM pg_catalog.pg_views
        WHERE schemaname IN (
        {%- if schema is iterable and (var is not string and var is not mapping) -%}
          {%- for s in schema -%}
            '{{ s }}'{% if not loop.last %},{% endif %}
          {%- endfor -%}
        {%- elif schema is string -%}
          '{{ schema }}'
        {%- endif -%}
      )) AS current
    LEFT JOIN (
      {%- for node in graph.nodes.values() | selectattr("resource_type", "equalto", "model") | list
                    + graph.nodes.values() | selectattr("resource_type", "equalto", "seed")  | list %}
        SELECT
        '{{node.schema}}' AS schema_name
         ,'{{node.name}}' AS ref_name
        {% if not loop.last %} UNION ALL {% endif %}
      {%- endfor %}
    ) AS desired on desired.schema_name = current.schema_name
                and desired.ref_name    = current.ref_name
    WHERE desired.ref_name is null
  {% endcall %}
  {% set result = load_result('get_orphaned_tables')['data'] %}
  {% if result %}
      {%- for to_delete in result %}
        {% call statement() -%}
            {% if dry_run %}
                {% do log('To be dropped: ' ~ to_delete[2] ~ ' ' ~ to_delete[0] ~ '.' ~ to_delete[1], True) %}
                SELECT
                    '{{ to_delete[2] }}'
                    , '{{ to_delete[0] }}'
                    , '{{ to_delete[1] }}';
            {% else %}
                {% do log('Dropping ' ~ to_delete[2] ~ ' ' ~ to_delete[0] ~ '.' ~ to_delete[1], True) %}
                DROP {{ to_delete[2] }} IF EXISTS "{{ to_delete[0] }}"."{{ to_delete[1] }}" CASCADE;
                {% do log('Dropped ' ~ to_delete[2] ~ ' ' ~ to_delete[0] ~ '.' ~ to_delete[1], True) %}
            {% endif %}
        {%- endcall %}
      {%- endfor %}
  {% else %}
    {% do log('No orphan tables to clean.', True) %}
  {% endif %}
{% endmacro %}
```