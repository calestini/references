---
title: Get Running Queries
description: 
published: true
date: 2022-10-20T18:50:40.379Z
tags: 
editor: markdown
dateCreated: 2022-10-20T18:50:37.451Z
---

# Get Running Queries

```sql
select userid , query , pid , starttime , text from stv_inflight order by starttime desc;
```