---
title: Terminate Running Queries
description: 
published: true
date: 2022-10-20T18:51:58.262Z
tags: 
editor: markdown
dateCreated: 2022-10-20T18:51:55.375Z
---

# Terminate Running Queries


```sql
select pg_terminate_backend(pid);
```