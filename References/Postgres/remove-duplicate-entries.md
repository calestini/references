---
title: Remove Duplicate Entries
description: 
published: true
date: 2023-02-03T03:12:55.078Z
tags: 
editor: markdown
dateCreated: 2023-02-03T03:12:31.252Z
---

# Remove Duplicate Entries


```sql
DELETE FROM search_leads
WHERE id IN (
  SELECT id FROM (
    SELECT id, ROW_NUMBER() OVER(PARTITION BY search_run_id, profile_id ORDER BY created_on ASC) AS row_num
    FROM search_leads
  ) lang
  WHERE lang.row_num > 1
)
```