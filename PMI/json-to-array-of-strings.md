---
title: JSON to array of strings
description: 
published: true
date: 2023-04-04T01:28:28.236Z
tags: 
editor: markdown
dateCreated: 2023-04-04T01:28:25.420Z
---

# JSON to Array of Strings

```js
/**
 * Method to extract all properties from a JSON into an array of dot-separated string
 * @param initialObj {object}
 * @returns {array}
 */
function jsonToArrayOfStrings(initialObj) {
  const results = [];

  function recursiveLogObj(obj, location = '') {
    let loc;
    // eslint-disable-next-line no-restricted-syntax
    for (const k of Object.keys(obj)) {
      loc = location ? `${location}.${k}` : k;
      if (obj[k] && typeof obj[k] === 'object') {
        recursiveLogObj(obj[k], loc);
      } else {
        // if string, number, array
        results.push(loc);
      }
    }
    return results;
  }

  return recursiveLogObj(initialObj, '');
}

```