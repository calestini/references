---
title: Polymorphic Documents
description: 
published: true
date: 2022-09-22T03:57:02.485Z
tags: 
editor: markdown
dateCreated: 2022-09-22T03:56:58.675Z
---

# Polimorphic Documents

Biggest issue: creating a file and being able to see it under file view. In theory we could just create a document in the `doc` table, but in practice there aver over 40 tables connected to the `doc` table.


## Work Tracking

```mermaid
flowchart TD
CreateAnalysisModal --> initializeUpload
initializeUpload --> processUpload
```

- /v1/files/initialize-upload

## Tables Connected to DocID

```sql
SELECT
    table_name,
    column_name,
    data_type,
    ordinal_position
FROM  INFORMATION_SCHEMA.COLUMNS
WHERE table_schema = 'pmi'     -- the database you want to search
  AND column_name = 'doc_id'    -- or: column_name LIKE '%name%'
  AND table_name NOT LIKE 'v_%';
```

| TABLE\_NAME | COLUMN\_NAME | DATA\_TYPE | ORDINAL\_POSITION |
| :--- | :--- | :--- | :--- |
| analysis | doc\_id | int | 1 |
| analysis\_rule | doc\_id | int | 1 |
| assembled\_annotation | doc\_id | int | 1 |
| assembled\_molecule | doc\_id | int | 1 |
| base\_molecule | doc\_id | int | 1 |
| deconv\_mass | doc\_id | int | 1 |
| doc\_analysis | doc\_id | int | 2 |
| doc\_history | doc\_id | int | 2 |
| doc\_md\_custom | doc\_id | int | 2 |
| doc\_md\_reserved | doc\_id | int | 2 |
| doc\_ref | doc\_id | int | 2 |
| doc\_upload | doc\_id | int | 7 |
| dq\_queue | doc\_id | int | 2 |
| feature\_ms\_xic | doc\_id | int | 1 |
| flat\_table\_byologic | doc\_id | int | 1 |
| flat\_table\_byomap | doc\_id | int | 1 |
| flat\_table\_intact | doc\_id | int | 1 |
| intact\_option | doc\_id | int | 1 |
| job\_doc | doc\_id | int | 3 |
| lucas\_mz\_molecule | doc\_id | int | 1 |
| match\_base | doc\_id | int | 1 |
| match\_feature | doc\_id | int | 1 |
| match\_feature\_attribute | doc\_id | int | 1 |
| match\_mass | doc\_id | int | 1 |
| match\_ms2 | doc\_id | int | 1 |
| modification\_molecule | doc\_id | int | 1 |
| molecule\_assembled | doc\_id | int | 1 |
| molecule\_base | doc\_id | int | 1 |
| molecule\_building\_block | doc\_id | int | 1 |
| molecule\_modification | doc\_id | int | 1 |
| observation\_base | doc\_id | int | 1 |
| observation\_decon\_mass | doc\_id | int | 1 |
| observation\_mz | doc\_id | int | 1 |
| protein\_annotation | doc\_id | int | 1 |
| sample | doc\_id | int | 1 |
| sample\_domain | doc\_id | int | 1 |
| sample\_mapping\_digest\_feature | doc\_id | int | 1 |
| sample\_property | doc\_id | int | 1 |
| sample\_rule\_match | doc\_id | int | 1 |
| sequence\_coverage | doc\_id | int | 1 |
| slice\_base | doc\_id | int | 1 |
| slice\_mz | doc\_id | int | 1 |
| slice\_trace | doc\_id | int | 1 |
| slice\_trace\_property | doc\_id | int | 1 |

