---
title: List Files
description: 
published: true
date: 2022-10-12T20:48:28.797Z
tags: 
editor: markdown
dateCreated: 2022-10-12T20:48:05.567Z
---

# List Files

```js
  /**
   * List all files in a "directory"
   * @param dir {string} - Directory with trailing path `/`
   */
  listFiles(dir){
    let results = [];
    return new Promise(function(resolve, reject) {
      if (TYPE === AWS) {
        // https://docs.aws.amazon.com/AmazonS3/latest/API/API_ListObjectsV2.html
        try {
          aws.config.update({region: config.server_platform.aws.region});
          let s3 = new aws.S3({apiVersion: CONSTANTS.API_VERSIONS.AWS_S3});
          let params = {
            Bucket: config.server_platform.aws.bucket,
            Prefix: dir
          };
          s3.listObjects(params, function(err, data) {
            if (err) {
              resolve(false);
            } else {
              resolve(data);
            }
          });
        } catch (error) {
          log.error({key: key, err: error}, 'file-store.listFiles');
          reject(error);
        }
      } else if (TYPE === AZURE) {
        // https://stackoverflow.com/questions/63327816/get-list-of-blobs-in-subfolder-azure
        try {
          const bsc = BlobServiceClient.fromConnectionString(config.server_platform.azure.connection_string);
          const cc = bsc.getContainerClient(config.server_platform.azure.container);
          const bbc = cc.listBlobsFlat({ prefix: dir });
          for (const item of bbc) {
            results.push(item.name);
          }
          console.log('results', results);
          resolve(results);
        } catch (error) {
          log.error({source: source, err: error}, 'file-store.listFiles');
          reject(error);
        }
      } else if (TYPE === GCLOUD) {
        // https://cloud.google.com/storage/docs/samples/storage-list-files#storage_list_files-nodejs
        try {
          const storage = new Storage({keyFilename: config.server_platform.gcloud.key_file});
          const bucket = storage.bucket(config.server_platform.gcloud.bucket);
          const [files] = bucket.listFiles(dir);
          files.forEach(file => {
            results.push(file.name);
            console.log(file.name);
          });
          resolve(results);
        } catch (error) {
          log.error({source: source, err: error}, 'file-store.listFiles');
          reject(error);
        }
      } else if (TYPE === ONPREM) {
        try {
          let folder = path.join(BASE_DIR, dir);
          let files = fs.readdirSync(folder, {withFileTypes: true} );
          files.forEach(file => {
            results.push(file.name);
            console.log(file.name);
          });
          resolve(results);
        } catch (error) {
          log.error({source: source, err: error}, 'file-store.listFiles');
          reject(error);
        }
      }
    })
  }
```