---
title: Subscriptions
description: Stripe subscription notes
published: true
date: 2022-10-12T17:24:16.343Z
tags: 
editor: markdown
dateCreated: 2022-10-12T16:16:18.912Z
---

# Subscriptions

## Descriptions

TBD

## Deleting Subscriptions Manually

- Delete entry from subscriptions page
- Remove info from organizations page 
- Delete on Stripe the subscirptions
- Set leads to 0