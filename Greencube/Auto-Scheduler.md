---
title: Auto-Scheduler
description: 
published: true
date: 2022-09-26T17:35:22.909Z
tags: 
editor: markdown
dateCreated: 2022-09-22T16:11:55.436Z
---

# Auto-Scheduler

The auto scheduler is an option in Greencube to auto execute steps for deployments. It only works for emails currently.

There are a few pending questions about auto-scheduler which can bring complications as the current implementation (V0.5):
- Auto-scheduler applies to all deployments for a user
- It is currently executing scheduled and overdue steps
- It only executed `is_next` step, which is not the same as `always` execute


## Checking in db if tasks need to be deployed

```sql
SELECT
  schedule_deployment_lead_steps.created_on AS schedule_deployment_lead_steps_created_on,
  schedule_deployment_lead_steps.updated_on AS schedule_deployment_lead_steps_updated_on,
  schedule_deployment_lead_steps.created_by AS schedule_deployment_lead_steps_created_by,
  schedule_deployment_lead_steps.id AS schedule_deployment_lead_steps_id,
  schedule_deployment_lead_steps.schedule_deployment_lead_id AS schedule_deployment_lead_steps_schedule_deployment_lead_i_1,
  schedule_deployment_lead_steps.schedule_message_id AS schedule_deployment_lead_steps_schedule_message_id,
  schedule_deployment_lead_steps.previous_lead_step_id AS schedule_deployment_lead_steps_previous_lead_step_id,
  schedule_deployment_lead_steps.deploy_on AS schedule_deployment_lead_steps_deploy_on,
  schedule_deployment_lead_steps.deployed_on AS schedule_deployment_lead_steps_deployed_on,
  schedule_deployment_lead_steps.status AS schedule_deployment_lead_steps_status,
  schedule_deployment_lead_steps.is_active AS schedule_deployment_lead_steps_is_active,
  schedule_deployment_lead_steps.manually_deployed AS schedule_deployment_lead_steps_manually_deployed,
  schedule_deployment_lead_steps.days_after_first_step AS schedule_deployment_lead_steps_days_after_first_step,
  schedule_deployment_lead_steps.days_after_previous_step AS schedule_deployment_lead_steps_days_after_previous_step,
  schedule_deployment_lead_steps.previous_step_responded AS schedule_deployment_lead_steps_previous_step_responded,
  schedule_deployment_lead_steps.execution_type AS schedule_deployment_lead_steps_execution_type,
  schedule_deployment_lead_steps.execution_index AS schedule_deployment_lead_steps_execution_index,
  schedule_deployment_lead_steps.medium_name AS schedule_deployment_lead_steps_medium_name,
  schedule_deployment_lead_steps.medium_id AS schedule_deployment_lead_steps_medium_id,
  schedule_deployment_lead_steps.step_type_name AS schedule_deployment_lead_steps_step_type_name,
  schedule_deployment_lead_steps.step_type_id AS schedule_deployment_lead_steps_step_type_id,
  schedule_deployment_lead_steps.subject AS schedule_deployment_lead_steps_subject,
  schedule_deployment_lead_steps.body AS schedule_deployment_lead_steps_body,
  schedule_deployment_lead_steps.attachments AS schedule_deployment_lead_steps_attachments,
  schedule_deployment_lead_steps.strategy AS schedule_deployment_lead_steps_strategy,
  schedule_deployment_lead_steps.deployment_name AS schedule_deployment_lead_steps_deployment_name,
  schedule_deployment_lead_steps.deployment_id AS schedule_deployment_lead_steps_deployment_id,
  schedule_deployment_lead_steps.schedule_name AS schedule_deployment_lead_steps_schedule_name,
  schedule_deployment_lead_steps.schedule_id AS schedule_deployment_lead_steps_schedule_id,
  schedule_deployment_lead_steps.schedule_step_name AS schedule_deployment_lead_steps_schedule_step_name,
  schedule_deployment_lead_steps.schedule_step_id AS schedule_deployment_lead_steps_schedule_step_id,
  schedule_deployment_lead_steps.previous_step_id AS schedule_deployment_lead_steps_previous_step_id,
  schedule_deployment_lead_steps.message_variation_name AS schedule_deployment_lead_steps_message_variation_name,
  schedule_deployment_lead_steps.message_variation_id AS schedule_deployment_lead_steps_message_variation_id,
  schedule_deployment_lead_steps.conditions AS schedule_deployment_lead_steps_conditions,
  schedule_deployment_lead_steps.previous_id AS schedule_deployment_lead_steps_previous_id,
  schedule_deployment_lead_steps.next_id AS schedule_deployment_lead_steps_next_id,
  schedule_deployment_lead_steps.is_next AS schedule_deployment_lead_steps_is_next,
  schedule_deployment_lead_steps.message_id AS schedule_deployment_lead_steps_message_id,
  schedule_deployment_lead_steps.thread_id AS schedule_deployment_lead_steps_thread_id,
  schedule_deployment_lead_steps.conversation AS schedule_deployment_lead_steps_conversation,
  schedule_deployment_lead_steps.first_responded_on AS schedule_deployment_lead_steps_first_responded_on,
  schedule_deployment_lead_steps.last_responded_on AS schedule_deployment_lead_steps_last_responded_on,
  schedule_deployment_lead_steps.responded AS schedule_deployment_lead_steps_responded,
  schedule_deployment_lead_steps.probability_score AS schedule_deployment_lead_steps_probability_score,
  schedule_deployment_lead_steps.lead_interest AS schedule_deployment_lead_steps_lead_interest,
  schedule_deployment_lead_steps.lead_interest_id AS schedule_deployment_lead_steps_lead_interest_id,
  schedule_deployment_lead_steps.lead_stage_id AS schedule_deployment_lead_steps_lead_stage_id
FROM
  schedule_deployment_lead_steps
  JOIN schedule_deployment_leads ON schedule_deployment_leads.id = schedule_deployment_lead_steps.schedule_deployment_lead_id
  JOIN schedule_deployments ON schedule_deployments.id = schedule_deployment_leads.deployment_id
  JOIN schedules ON schedules.id = schedule_deployments.schedule_id
  JOIN users ON users.id = schedules.user_id
  JOIN schedule_settings ON users.id = schedule_settings.user_id
WHERE
  date_trunc('day',  schedule_deployment_lead_steps.deploy_on) <= date_trunc('day', now())
  AND schedule_deployments.is_archived IS false
  AND schedule_settings.auto_deploy_emails IS true
  AND (
    schedule_deployment_lead_steps.is_next IS true
  )
  AND schedule_deployment_lead_steps.medium_name = 'Email'
  AND schedule_deployment_lead_steps.status in ('overdue', 'schedule')
  AND EXTRACT(hour FROM timezone(schedule_settings.default_timezone, now() ) ) = 
  	EXTRACT(hour FROM schedule_settings.default_email_time)
--
```